import csv
from water_quality_map.models import Water

# Ajouter les données du fichier CSV à la base de données
# Appeler la fonction dans l'interpréteur python
def add_data():
    # Chemin du fichier CSV
    fichier = 'C:/Users/Marie-Hélène/Documents/Université/Géomatique/SessionHiver2024/GMQ580/Projet_Session/water_quality_map_project/data/rsma-qualo-qualite-eau-rive-depuis-2012.csv'
    # Message de retour
    msg = 'Les données ont été ajoutées avec succès'
    # Si la base de données est vide, ajoutez les données du fichier CSV
    if Water.objects.count() == 0:
        # Créer les objets de la base de données
        # Ouvrir le fichier CSV et lire les données
        with open(fichier, newline='', encoding='utf-8') as csvfile:
            reader = csv.reader(csvfile)
            next(reader)  # Ignorer la première ligne si elle contient des en-têtes
            # Comme le fichier csv contient beaucoup de données, on va juste ajouter les 600 premières lignes
            # sqlite3 ne peut pas gérer un grand nombre de données
            cmpt = 0
            for row in reader:
                if cmpt == 600: # On peut changer le nombre de lignes à ajouter mais à enlever si meilleur bd
                    break
                # Traitez chaque ligne du fichier CSV et ajoutez les données à la base de données
                # Exemple d'ajout de données dans la base de données :
                try:
                    Water.objects.create(
                        point_echantillonnage=row[0],
                        plan_eau=row[1],
                        administration=row[2],
                        nom=row[3],
                        date_heure=row[4],
                        temperature=row[5],
                        pH=row[6],
                        coliformes_fecaux=row[7],
                        prec_tot_60h=row[8],
                        latitude=row[9],
                        longitude=row[10]
                    )
                    cmpt += 1
                # Gérer les erreurs d'ajout de données
                except:
                    # Mettre dans un fichier csv
                    with open('data/erreurs.csv', 'a', newline='', encoding='utf-8') as csvfile:
                        writer = csv.writer(csvfile)
                        writer.writerow(row)
                    continue

    else:    
        msg = 'La base de données contient déjà des données'
    print(msg)
