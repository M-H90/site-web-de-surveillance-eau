import json
from datetime import datetime
from django.shortcuts import render, get_object_or_404, redirect
from django.template import loader
from django.http import HttpResponse
from .models import Water
from .forms import myForm
# Create your views here.

# view de la liste des objets
def list_data(request):
    # Récupérer les données
    data = Water.objects.all().values()
    template = loader.get_template('list_data.html')
    context = {
        'data': data,
    }
    # Rendre le template avec les données
    return HttpResponse(template.render(context, request))

# view du detail d'un objet
def detail_data(request, objet_id):
    # Récupérer l'objet
    data = get_object_or_404(Water, pk=objet_id)
    # Rendre le template avec l'objet
    return render(request, 'detail_data.html', {'data': data})

# view de la creation d'un objet
def create_data(request):
    # Si la requête est de type POST, traiter les données du formulaire
    if request.method == 'POST':
        # Créer une instance du formulaire et le remplir avec les données de la requête
        formulaire = myForm(request.POST)
        # Valider le formulaire
        if formulaire.is_valid():
            # Sauvegarder les données dans la base de données
            nouvel_objet = formulaire.save()
            return redirect('detail_data', objet_id=nouvel_objet.pk)
    else:
        formulaire = myForm()
    # Rendre le template avec le formulaire
    return render(request, 'create_data.html', {'formulaire': formulaire})

# view de la page d'accueil
def index(request):
    # Récupérer l'année, le mois et le jour de la requête
    year = request.GET.get('annee') if request.GET.get('annee') else None
    month = request.GET.get('mois') if request.GET.get('mois') else None
    day = request.GET.get('jour') if request.GET.get('jour') else None
    # Récupérer les données
    data = Water.objects.all()
    # Filtrer les données
    if year != 'All' and year:
        data = data.filter(date_heure__year=year)

    if month != 'All' and month:
        data = data.filter(date_heure__month=month)

    if day != 'All' and day:
        data = data.filter(date_heure__day=day)
    serialized_data = []
    # Transformer les données en dictionnaire pour format json
    for item in data:
        serialized_item = {
            'point_echantillonnage': item.point_echantillonnage,
            'plan_eau': item.plan_eau,
            'administration': item.administration,
            'nom': item.nom,
            'date_heure': datetime.fromisoformat(str(item.date_heure)).strftime("%d-%m-%Y %H:%M:%S") if item.date_heure else '', # Convertir en str
            'temperature': str(item.temperature) if item.temperature else '', # Convertir en str
            'pH': str(item.pH) if item.pH else '', # Convertir en str
            'coliformes_fecaux': str(item.coliformes_fecaux) if item.coliformes_fecaux else '', # Convertir en str
            'prec_tot_60h': str(item.prec_tot_60h) if item.prec_tot_60h else '', # Convertir en str
            'latitude': str(item.latitude) if item.latitude else '', # Convertir en str
            'longitude': str(item.longitude) if item.longitude else '', # Convertir en str
        }
        # Ajouter l'élément sérialisé à la liste
        serialized_data.append(serialized_item)

    # Convertir les données en JSON
    json_data = json.dumps(serialized_data)
    # Rendre le template avec les données
    return render(request, 'index.html', {'data': json_data})

