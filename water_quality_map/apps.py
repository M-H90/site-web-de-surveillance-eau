from django.apps import AppConfig


class WaterQualityMapConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    # Nom de l'application
    name = 'water_quality_map'
