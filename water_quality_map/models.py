from django.db import models

# Create your models here.
class Water(models.Model):
    point_echantillonnage = models.CharField(max_length=20)
    plan_eau = models.CharField(max_length=100)
    administration =models.CharField(max_length=50)
    nom = models.CharField(max_length=50)
    date_heure = models.DateTimeField()
    temperature = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)
    pH = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)
    coliformes_fecaux = models.IntegerField(blank=True, null=True)
    prec_tot_60h = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True) 
    latitude = models.DecimalField(max_digits=30, decimal_places=20, blank=True, null=True)
    longitude = models.DecimalField(max_digits=30, decimal_places=20, blank=True, null=True)
    class Meta:
        app_label = 'water_quality_map'
        # Contrainte d'unicité
        constraints = [
            models.UniqueConstraint(fields=['point_echantillonnage', 'date_heure'], name='unique_nom_date')
        ]

    def __str__(self):
        return self.point_echantillonnage
