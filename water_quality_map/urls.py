from django.urls import path
from . import views

# Créer les urls
urlpatterns = [
    path('', views.index, name='index'),
    path('list_data/', views.list_data, name='list_data'),
    path('detail/<int:objet_id>/', views.detail_data, name='detail_data'),
    path('create_data/', views.create_data, name='create_data')]