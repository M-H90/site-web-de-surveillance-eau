from django import forms
from .models import Water


# Formulaire basé sur un modèle
class myForm(forms.ModelForm):
    class Meta:
        model = Water  # Modèle utilisé pour le formulaire
        fields = ['point_echantillonnage', 'plan_eau', 'administration', 'nom', 
                'date_heure', 'temperature',
                'pH', 'coliformes_fecaux', 'prec_tot_60h',
                'latitude','longitude']  # Liste des champs à inclure dans le formulaire
        widgets = { # Personnalisation des widgets
            'date_heure': forms.DateTimeInput(attrs={'type': 'datetime-local'}),
            'temperature': forms.NumberInput(attrs={'step': '0.01'}),
            'pH': forms.NumberInput(attrs={'step': '0.01'}),
            'prec_tot_60h': forms.NumberInput(attrs={'step': '0.01'}),
            'latitude': forms.NumberInput(),
            'longitude': forms.NumberInput()
        }