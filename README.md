# Site web de surveillance eau

Ce site a été conçu pour projet de fin de session pour le cours de géoinformatique II (gmq580) à l'UDS. Il affiche une cartographie des données de la qualité de l'eau à Montréal. Les données des valeurs des coliformes des échantilonnages de 2012 à 2023 sont affichés sur la carte. Il y a en plus un tableau de bord qui affiche les informations de la donnée sélectionnée. Ce projet utilise le framework Django.

## Limites du projet

Comme c'est un projet universitaire il a eu un temps limité pour le faire, alors il y a plusieurs améliorations à faire. La base de donnée est en SQLite3 donc le nombre de données est très limité. Pour le moment, il est simplement possible d'ajouter une donnée à la fois par un formualaire dans le site. Il n'est pas possible de supprimer une donnée. Le tableau de bord pourrait être amélioré par l'ajout de statisques ou de graphiques.


## Liste de commandes utiles
Ouvrir l'interpréteur Python (Shell)
```
py manage.py shell
```
Fermer l'interpréteur Python
```
exit()
```
Afficher les données (objets Water)
```
from water_quality_map.models import Water
Water.objects.all()
```
Ajouter données dans la base de données par csv. Ne pas oublier de changer le chemin du fichier csv dans le fichier python
```
from water_quality_map.add_data_csv import add_data

add_data() 
```

Voir les changements et les faire
```
python manage.py makemigrations
python manage.py migrate
```
 Lancer serveur et site web
```
python manage.py runserver
```

